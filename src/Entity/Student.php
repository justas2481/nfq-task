<?php

namespace App\Entity;

use App\Repository\StudentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StudentRepository::class)
 */
class Student
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=30, unique=true)
     */
    private string $fullName;

    /**
     * @ORM\ManyToOne(targetEntity=Group::class, inversedBy="students")
     */
    private ?Group $belongsToGroup = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $positionInGroup = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getBelongsToGroup(): ?Group
    {
        return $this->belongsToGroup;
    }

    public function setBelongsToGroup(?Group $belongsToGroup): self
    {
        $this->belongsToGroup = $belongsToGroup;

        return $this;
    }

    public function getPositionInGroup(): ?int
    {
        return $this->positionInGroup;
    }

    public function setPositionInGroup(?int $positionInGroup): self
    {
        $this->positionInGroup = $positionInGroup;

        return $this;
    }
}
