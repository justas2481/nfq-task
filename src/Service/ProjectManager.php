<?php

namespace App\Service;

use App\Entity\Group;
use App\Entity\Project;
use Doctrine\ORM\EntityManagerInterface;

final class ProjectManager
{
    private EntityManagerInterface $entityManager;
    private Project $project;
    private int $groupsPerProject;

    public function __construct(
        EntityManagerInterface $manager,
        Project $project,
        int $groupsPerProject
    ) {
        $this->entityManager = $manager;
        $this->project = $project;
        $this->setGroupsPerProject($groupsPerProject);
    }

    public function setGroupsPerProject(int $groupsPerProject): self
    {
        $this->groupsPerProject = $groupsPerProject;

        return $this;
    }

    public function getGroupsPerProject(): int
    {
        return $this->groupsPerProject;
    }

    public function createProject()
    {
        $this->entityManager->persist($this->project);
        $this->entityManager->flush();
        $this->createGroups();
    }

    private function createGroups()
    {
        for ($i = 0; $i < $this->getGroupsPerProject(); $i++) {
            $group = new Group();
            $group->setProject($this->project);
            $this->entityManager->persist($group);
        }
        
        $this->entityManager->flush();
    }
}
