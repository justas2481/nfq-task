<?php

namespace App\Form;

use App\Entity\Student;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class StudentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $fullNameConstraints = [
            new NotBlank([
                'message' => 'Name is mandatory.',
            ]),
            new Length([
                'min' => 4,
                'minMessage' => 'At least {{ limit }} chars are required.',
                'max' => 30,
                'maxMessage' => 'Name is too long.',
            ])
        ];

        $builder
            ->add('fullName', TextType::class, [
                'label' => 'Full name',
                'constraints' => $fullNameConstraints,
            ])
            ->add('submit', SubmitType::class, ['label' => 'Create']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Student::class,
        ]);
    }
}
