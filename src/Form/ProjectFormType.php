<?php

namespace App\Form;

use App\Entity\Project;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ProjectFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $titleConstraints = [
            new NotBlank([
                'message' => 'Title is a mandatory field.',
            ]),
            new Length([
                'min' => 4,
                'minMessage' => 'At least {{ limit }} chars for title are required.',
                'max' => 255,
                'maxMessage' => 'Title is too long.',
            ])
        ];

        $studentsPerGroupConstraints = [
            new NotBlank([
                'message' => 'Students per group is a mandatory field.',
            ]),
            new GreaterThanOrEqual(1),
            new LessThanOrEqual(10),
        ];

        $groupsPerProjectConstraints = [
            new NotBlank([
                'message' => 'Groups per project is a mandatory field.',
            ]),
            new GreaterThanOrEqual(2),
            new LessThanOrEqual(10),
        ];

        $builder
            ->add('title', TextType::class, [
                'label' => 'Project title',
                'constraints' => $titleConstraints,
            ])
            ->add('groupsPerProject', NumberType::class, [
                'label' => 'Groups per project',
                'mapped' => false,
                'constraints' => $groupsPerProjectConstraints,
            ])
            ->add('studentsPerGroup', NumberType::class, [
                'label' => 'Students per group',
                'constraints' => $studentsPerGroupConstraints,
            ])
            ->add('submit', SubmitType::class, ['label' => 'Create']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
        ]);
    }
}
