<?php

namespace App\Controller;

use App\Entity\Group;
use App\Form\GroupFormType;
use App\Repository\GroupRepository;
use App\Repository\StudentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GroupController extends AbstractController
{
    #[Route(
        '/group/assign-group/{groupId}/{position}',
        name: 'assign_group'
    )]
    public function assign(
        Request $request,
        EntityManagerInterface $entityManager,
        GroupRepository $groupRepo,
        StudentRepository $studentRepo,
        int $groupId,
        int $position
    ): Response {
        $group = $groupRepo->findOneBy(['id' => $groupId]);
        if (!$group) {
            return $this->redirectToRoute('manage_project');
        }

        $students = $studentRepo->findAll();
        if (!$students) {
            return $this->redirectToRoute('manage_project');
        }

        $project = $group->getProject();
        // Position is 0 based, not 1 based
        if ($position < 0 || $position > $project->getStudentsPerGroup() -1) {
            return $this->redirectToRoute('manage_project');
        }

        $studentsInGroupCount = count($group->getStudents()) ?? 0;

        $form = $this->createForm(GroupFormType::class, null, ['choices' => $students]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $student = $form->get('student')->getData();
            if (!$student) {
                return $this->redirectToRoute('manage_project');
            }

            if ($studentsInGroupCount >= $project->getStudentsPerGroup()) {
                return $this->redirectToRoute('manage_project');
            }

            // Security check if other student is assigned to particular group and position
            $otherStudentInGroup = $studentRepo->findOneBy(['positionInGroup' => $position, 'belongsToGroup' => $group]);
            if ($otherStudentInGroup) {
                return $this->redirectToRoute('manage_project');
            }

            $student->setBelongsToGroup($group);
            $student->setPositionInGroup($position);
            $entityManager->persist($student);
            $entityManager->flush();

            return $this->redirectToRoute('manage_project');
        }

        return $this->render('group/index.html.twig', [
            'controller_name' => 'GroupController',
            'group_form' => $form->createView(),
        ]);
    }
}
