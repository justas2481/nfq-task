<?php

namespace App\Controller;

use App\Entity\Student;
use App\Form\StudentFormType;
use App\Repository\StudentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StudentController extends AbstractController
{
    #[Route('/student', name: 'new_student')]
    public function index(
        Request $request,
        EntityManagerInterface $entityManager
    ): Response {
        $student = new Student();

        $form = $this->createForm(StudentFormType::class, $student);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($student);
            try {
                $entityManager->flush();

                return $this->redirectToRoute('manage_project');
            } catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e) {
                $form->addError(new FormError('This student is already exists in the system!'));
            }
        }

        return $this->render('student/index.html.twig', [
            'controller_name' => 'StudentController',
            'student_form' => $form->createView(),
        ]);
    }

    #[Route('/student/delete/{id}', name: 'delete_student')]
    public function delete(
        EntityManagerInterface $entityManager,
        StudentRepository $studentRepo,
        int $id
    ): Response {
        $student = $studentRepo->find($id);
        if (!$student) {
            throw $this->createNotFoundException('No student with given ID has been found.');
        }

        $entityManager->remove($student);
        $entityManager->flush();
        
        return $this->redirectToRoute('manage_project');
    }
}
