<?php

namespace App\Controller;

use App\Entity\Project;
use App\Form\ProjectFormType;
use App\Repository\ProjectRepository;
use App\Repository\StudentRepository;
use App\Service\ProjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProjectController extends AbstractController
{
    #[Route('/', name: 'new_project')]
    public function index(
        Request $request,
        EntityManagerInterface $manager,
        ProjectRepository $projectRepo
    ): Response {
        // Check if project exists
        // Since we aren't allowed to remove projects, this technique works
        if ($projectRepo->findOneBy(['id' => 1])) {
            return $this->redirectToRoute('manage_project');
        }

        $project = new Project();

        $form = $this->createForm(ProjectFormType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $projectManager = new ProjectManager(
                $manager,
                $project,
                (int) $form->get('groupsPerProject')->getData()
            );
            $projectManager->createProject();

            return $this->redirectToRoute('manage_project');
        }

        return $this->render('project/index.html.twig', [
            'controller_name' => 'ProjectController',
            'project_form' => $form->createView(),
        ]);
    }

    #[Route('/manage-project', name: 'manage_project')]
    public function manage(
        ProjectRepository $projectRepo,
        StudentRepository $studentRepo
    ): Response {
        // Check if project exists
        // Since we aren't allowed to remove projects, this technique works
        $project = $projectRepo->findOneBy(['id' => 1]);
        if (!$project) {
            return $this->redirectToRoute('new_project');
        }

        $students = $studentRepo->findAll();

        return $this->render('project/preview.html.twig', [
            'controller_name' => 'ProjectController',
            'project' => $project,
            'students' => $students,
        ]);
    }
}
