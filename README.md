Task for NFQ (app to assign students in groups for a project)

This is a simple CRUD app that implements rquirements of a given task to demonstrate OOP and MVC knowledge.

# Technical decisions

* Sollution was made in PHP (for back-end).
* Symfony was chosen as a rich and functional framework which particulary fits the requirements of a task.
* For a database I used Doctrine ORM. This lets to utilize functionality of a framework in tandem with automatically generated migrations to provide script for DB initialisation.
* Twig for a basic templating and minimalistic front-end.
* Composer is used to manage all the dependencies.
* All app is Dockerized to avoid separate Nginx server, MariaDB, PHPMyAdmin, PHP-FPM, etc.
* App uses entities to hold onto objects that reflect real database tables and their columns as object properties.
